## Build Setup

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

## link test
Front End Test: http://localhost:3000/login
Programming Test Input Number: http://localhost:3000/programing-test1
Programming Test Define Employee: http://localhost:3000/programing-test2

## FlowChart
![FlowChart](laundromat-system.jpg)
